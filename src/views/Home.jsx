<Window>
	<View>
    <Illustration height={200} image={require('../assets/images/icon_ame.svg')}/>
    <Header textAlign="center">Teste de Header</Header>
    <Spacing size="lg" />
    <ListView items={this.state.links} onItemSelect={this.navigateTo} />
  </View>
</Window>
