module.exports = {
    "name": "avec",
    "title": "avec",
    "slug": "avec",
    "organization": {
        "name": "avec",
        "owner": {
            "email": "pedro.henryke@outlook.com"
        }
    },
    "public-key": "cbe15010-787b-4ae9-895f-ae2db2e1acdd",
    "ame-super-app-client": "2.9.0",
    "ame-miniapp-components": "2.6.1",
    "version": "0.1.0"
}